import { Component } from "react";

class SetTime extends Component{

    constructor (props){
        super(props);
        
        let today  = new Date().toLocaleString()

        this.state = ({
            currentTime: today,
            outPut:[]
        })
    }

    btnClick=()=>{
        console.log("btn được ấn")
        this.setState({
            outPut:[...this.state.outPut, this.state.currentTime]
        })
    }


    render(){
        return(
            <>
            <div className="form-control mt-3" style={{backgroundColor:"Highlight"}}>
                <p style={{color:"white"}}>Time is: {this.state.currentTime}</p>
            </div>
            <div className="form-control mt-3 bg-info">
                
                    {this.state.outPut.map((value,index)=>{
                        return <p key={index}>
                                {value}
                        </p>
                    })}
            
            </div>
            <div className="row mt-3">
                <button className="btn btn-success" onClick={this.btnClick}>Set Time</button>
            </div>
            </>
        )
    }
}

export default SetTime